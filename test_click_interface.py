import click
from click.testing import CliRunner

import serial_validator

# This is more of an integration test as it actually uses stdin/stdout
def test_serial_validator():
    runner = CliRunner()
    result = runner.invoke(serial_validator.validate, ['20161226'])
    assert result.exit_code == 0


def test_serial_validator_with_revision():
    runner = CliRunner()
    result = runner.invoke(serial_validator.validate, ['20161226.5'])
    assert result.exit_code == 0
