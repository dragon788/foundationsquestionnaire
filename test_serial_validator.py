import pytest
import serial_validator


def test_numeric_serial():
    result = serial_validator.validate_numeric_serial("20161225")
    assert result == True


def test_numeric_serial_with_revision():
    result = serial_validator.validate_numeric_serial("20161225.5")
    assert result == True


def test_non_numeric_serial():
    with pytest.raises(ValueError):
        serial_validator.validate_numeric_serial("A0161225")


def test_numeric_serial_with_bad_separator():
    with pytest.raises(ValueError):
        serial_validator.format_date("20161225-5")


def test_numeric_serial_with_another_bad_separator():
    with pytest.raises(ValueError):
        serial_validator.format_date("20161225,5")
