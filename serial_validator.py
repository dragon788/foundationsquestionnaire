import click
import datetime
import re

VALID_INPUT_FORMATS = ("YYYYMMDD","YYYYMMDD.#")

@click.command()
@click.argument('serial')
def validate(**kwargs):
    test_serial = kwargs['serial']
    validate_numeric_serial(test_serial)


def validate_numeric_serial(test_serial):
    year, month, day = format_date(test_serial)
    return check_date(year, month, day)


def format_date(test_serial):
    if len(test_serial) == 8:
        return (
            test_serial[0:4],
            test_serial[5:6],
            test_serial[7:8]
            )
    else:
        try:
            date, revision  = test_serial.split('.')
            check_revision(revision)
            return format_date(date)
        except ValueError:
            print("Not in valid input format of {}".format(tuple(VALID_INPUT_FORMATS)))
            raise 


def check_revision(revision):
    if not re.match(r'^([0-9]{1})$', revision):
        raise "Non-numeric revision is invalid"


def check_date(year, month, day):
    try:
        attempt_date = datetime.datetime(int(year), int(month), int(day))
        return True
    except ValueError:
        print("Invalid date detected")
        raise


if __name__ == '__main__':
    validate()
