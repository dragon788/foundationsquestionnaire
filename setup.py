from setuptools import setup

setup(
        name='serial_validator',
        version='0.1',
        py_modules=['serial_validator'],
        install_requires=[
            'Click',
        ],
        entry_points='''
            [console_scripts]
            serial_validator=serial_validator:validate
            ''',
    )
